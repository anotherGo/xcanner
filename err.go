package main

import "errors"

var errUnkownParentDir = errors.New("errUnkownParentDir")
var errNotAbsDir = errors.New("errNotAbsDir")
