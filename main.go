package main

import (
	"fmt"
	"os"
	"path"
	"regexp"

	"gitea.com/anotherGo/aerr"
)

type FileInfo struct {
	os.FileInfo
	dir string
}

func (fi FileInfo) Dir() string {
	return fi.dir
}
func (fi FileInfo) BaseName() string {
	return path.Base(fi.Name())
}
func (fi FileInfo) Path() string {
	return path.Join(fi.Dir(), fi.BaseName())
}

type errorOrFileInfo struct {
	err error
	fi  FileInfo
}
type Cursor struct {
	ch chan (errorOrFileInfo)
}

func (c *Cursor) Next() (FileInfo, error) {
	var res errorOrFileInfo
	go func() {
		defer aerr.Ignore(func(err interface{}) {
			res.err = err.(error)
		})
		res = <-c.ch
	}()
	return res.fi, res.err
}
func Scan(dir string, pattren *regexp.Regexp) (*Cursor, error) {
	if path.IsAbs(dir) == false {
		return nil, errNotAbsDir
	}
	ch := make(chan (errorOrFileInfo))
	go scan(dir, ch, pattren)
	return &Cursor{ch}, nil
}

func scan(dir string, ch chan (errorOrFileInfo), pattern *regexp.Regexp) {
	files, err := readdir(dir)
	if err != nil {
		ch <- errorOrFileInfo{err: err}
		return
	}
	for _, file := range files {
		sendIfMatch(file, ch, pattern)
		if file.IsDir() {
			scan(dir, ch, pattern)
		}
	}
}
func sendIfMatch(args ...interface{}) {
	if args[0].(*regexp.Regexp).Match([]byte(args[0].(FileInfo).Path())) {
		args[1].(chan (errorOrFileInfo)) <- errorOrFileInfo{nil, args[0].(FileInfo)}
	}
}
func readdir(dir string) ([]FileInfo, error) {
	folder, err := os.Open(dir)
	defer folder.Close()
	if err != nil {
		return nil, err
	}
	osFiles, err := folder.Readdir(-1)
	if err != nil {
		return nil, err
	}
	files := make([]FileInfo, len(osFiles))
	for i, file := range osFiles {
		files[i] = FileInfo{file, dir}
	}
	return files, nil
}
func main() {
	file, err := os.Open("/home/abgr")
	aerr.Panicerr(err, nil)
	files, err := file.Readdir(-1)
	aerr.Panicerr(err, nil)
	for _, file := range files {
		fmt.Println(file.Name())
	}
}
